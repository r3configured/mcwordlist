﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using MCWordlist.Properties;

//====================================================

namespace MCWordlist
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            mWords.Load();
            sequenceTextBox.Text = Settings.Default.sequence;
            propertyGrid1.SelectedObject = mWords;
        }

        //====================================================

        private void saveButton_Click(object sender, EventArgs e)
        {
            if(sequenceTextBox.Text.Length != 24)
            {
                MessageBox.Show("Sequence must be 24 digits.", "Sequence", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            mSequence = sequenceTextBox.Text;
            Settings.Default.sequence = mSequence;
            Settings.Default.Save();
            mWords.Save();

            if(!wordCheck())
            {
                MessageBox.Show("Wordcount does not match sequence.", "Sequence", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SaveFileDialog sd = new SaveFileDialog
            {
                Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true,
                DefaultExt = "txt",
                Title = "Save Wordlist"
            };

            if(sd.ShowDialog() == DialogResult.OK)
            {
                if(!String.IsNullOrEmpty(sd.FileName))
                {
                    using(StreamWriter file = new StreamWriter(sd.FileName))
                    {
                        resultLabel.Text = "Generating wordlist...";
                        resultLabel.Update();
                        prepare();
                        resultLabel.Text = "Done! Generated Tokens: " + writeTokens(file);
                    }
                }
            }
        }

        //====================================================

        private int writeTokens(StreamWriter file)
        {
            int tokenCount = 0;

            while(mTmpListLengths.Sum() > 0)
            {
                mTokenLine = mTokenLine2 = String.Empty;

                for(int i = 0; i < mSequenceIntList.Count; i++)
                {
                    int eCount = 0;

                    for(int j = 0; j < i; j++)
                    {
                        if(mSequenceIntList[j] == mSequenceIntList[i])
                        {
                            ++eCount;
                        }
                    }

                    mTokenLine += mTmpLists[mSequenceIntList[i]][mTmpListLengths[mSequenceIntList[i]]].ElementAt(eCount);

                    if(spaceDelimiterCheckBox.Checked)
                    {
                        mTokenLine2 += mTmpLists[mSequenceIntList[i]][mTmpListLengths[mSequenceIntList[i]]].ElementAt(eCount);

                        if(i < mSequenceIntList.Count - 1)
                        {
                            mTokenLine2 += " ";
                        }
                    }
                }

                file.WriteLine(mTokenLine);

                if(spaceDelimiterCheckBox.Checked)
                {
                    ++tokenCount;
                    file.WriteLine(mTokenLine2);
                }

                ++tokenCount;
                mTmpListLengths[0]--;
                traverse(0);
            }

            return tokenCount;
        }

        //====================================================

        private void traverse(int depth)
        {
            if(depth < mTmpLists.Count && mTmpListLengths[depth] == -1)
            {
                mTmpListLengths[depth] = mTmpLists[depth].Count - 1;
                mTmpListLengths[depth + 1]--;

                if(depth < mTmpLists.Count - 1)
                    traverse(depth + 1);
            }
        }

        //====================================================

        private void prepare()
        {
            mTokenLine = String.Empty;
            mTmpLists.Clear();
            mTmpListLengths.Clear();
            mSequenceIntList.Clear();

            mTmpLists.Add(mWords.Index0.Permute().ToList());
            mTmpLists.Add(mWords.Index1.Permute().ToList());
            mTmpLists.Add(mWords.Index2.Permute().ToList());
            mTmpLists.Add(mWords.Index3.Permute().ToList());
            mTmpLists.Add(mWords.Index4.Permute().ToList());
            mTmpLists.Add(mWords.Index5.Permute().ToList());
            mTmpLists.Add(mWords.Index6.Permute().ToList());
            mTmpLists.Add(mWords.Index7.Permute().ToList());
            mTmpLists.Add(mWords.Index8.Permute().ToList());
            mTmpLists.Add(mWords.Index9.Permute().ToList());
            
            for(int i = 0; i < 10; i++)
            {
                mTmpListLengths.Add(mTmpLists[i].Count() - 1);
            }

            foreach(char c in mSequence)
            {
                mSequenceIntList.Add(int.Parse(c.ToString()));
            }
        }

        //====================================================

        private bool wordCheck()
        {
            List<int> seqCounts = new List<int>(10);

            for(int i = 0; i < 10; i++)
            {
                int count = 0;

                foreach(char c in mSequence)
                {
                    if(i == int.Parse(c.ToString()))
                    {
                        ++count;
                    }
                }

                seqCounts.Add(count);
            }

            return seqCounts[0] == mWords.Index0.Count && seqCounts[1] == mWords.Index1.Count &&
                   seqCounts[2] == mWords.Index2.Count && seqCounts[3] == mWords.Index3.Count &&
                   seqCounts[4] == mWords.Index4.Count && seqCounts[5] == mWords.Index5.Count &&
                   seqCounts[6] == mWords.Index6.Count && seqCounts[7] == mWords.Index7.Count &&
                   seqCounts[8] == mWords.Index8.Count && seqCounts[9] == mWords.Index9.Count;
        }

        //====================================================

        private void sequenceTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || e.KeyChar==8);
        }

        //====================================================

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            mWords.Save();
            Settings.Default.sequence = sequenceTextBox.Text;
            Settings.Default.Save();
        }
        
        //====================================================

        private String mSequence;
        private readonly List<int> mSequenceIntList = new List<int>(24);
        private readonly List<int> mTmpListLengths = new List<int>(10);
        private String mTokenLine, mTokenLine2;
        private readonly List<List<IEnumerable<string>>> mTmpLists = new List<List<IEnumerable<string>>>(10);
        private readonly Words mWords = new Words();
    }
}



