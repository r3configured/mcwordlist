﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using MCWordlist.Properties;

//=====================================

namespace MCWordlist
{
    public class Words
    {
        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index0 { get => mIndex0; set => mIndex0 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index1 { get => mIndex1; set => mIndex1 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index2 { get => mIndex2; set => mIndex2 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index3 { get => mIndex3; set => mIndex3 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index4 { get => mIndex4; set => mIndex4 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index5 { get => mIndex5; set => mIndex5 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index6 { get => mIndex6; set => mIndex6 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index7 { get => mIndex7; set => mIndex7 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index8 { get => mIndex8; set => mIndex8 = value; }

        [Category("Words")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(CsvConverter))]
        public List<string> Index9 { get => mIndex9; set => mIndex9 = value; }

        //=====================================

        public void Load()
        {
            mIndex0 = Settings.Default.words0.Cast<string>().ToList();
            mIndex1 = Settings.Default.words1.Cast<string>().ToList();
            mIndex2 = Settings.Default.words2.Cast<string>().ToList();
            mIndex3 = Settings.Default.words3.Cast<string>().ToList();
            mIndex4 = Settings.Default.words4.Cast<string>().ToList();
            mIndex5 = Settings.Default.words5.Cast<string>().ToList();
            mIndex6 = Settings.Default.words6.Cast<string>().ToList();
            mIndex7 = Settings.Default.words7.Cast<string>().ToList();
            mIndex8 = Settings.Default.words8.Cast<string>().ToList();
            mIndex9 = Settings.Default.words9.Cast<string>().ToList();
        }

        //=====================================

        public void Save()
        {
            StringCollection collection = new StringCollection();

            collection.AddRange(mIndex0.ToArray());
            Settings.Default.words0 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex1.ToArray());
            Settings.Default.words1 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex2.ToArray());
            Settings.Default.words2 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex3.ToArray());
            Settings.Default.words3 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex4.ToArray());
            Settings.Default.words4 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex5.ToArray());
            Settings.Default.words5 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex6.ToArray());
            Settings.Default.words6 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex7.ToArray());
            Settings.Default.words7 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex8.ToArray());
            Settings.Default.words8 = collection;
            Settings.Default.Save();

            collection = new StringCollection();
            collection.AddRange(mIndex9.ToArray());
            Settings.Default.words9 = collection;
            Settings.Default.Save();
        }

        //=====================================
        
        private List<String> mIndex0 = new List<string>();
        private List<String> mIndex1 = new List<string>();
        private List<String> mIndex2 = new List<string>();
        private List<String> mIndex3 = new List<string>();
        private List<String> mIndex4 = new List<string>();
        private List<String> mIndex5 = new List<string>();
        private List<String> mIndex6 = new List<string>();
        private List<String> mIndex7 = new List<string>();
        private List<String> mIndex8 = new List<string>();
        private List<String> mIndex9 = new List<string>();
    }
}